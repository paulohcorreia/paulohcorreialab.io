

---

# Paulo Henrique Correia 
### Website

Strategies and tools for an up-to-date organization.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [My Projects](#my-projects)
- [My workflow](#my-workflow)
- [Clients](#clients)
- [Blog](#blog)
- [Contact](#contact)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## My Projects
_More Coming Soon._
- Casa Brasil Torino

## My workflow
_Coming Soon._

## Clients
_Coming Soon._

## Blog
_Coming Soon._
- paulohcorreia.gitlab.io

## Contact
_More Coming Soon._


