## Paulo Henrique Correia

On this channel you'll find a summary of the projects I have been working on. This page offers you a glimpse of some experimentation on planning and deploying a business, the hullabaloos on tech, italian tastes and brazilian sounds.

Follow the hashtag #readytostart to hear some of my experience on modern strategies on business development on the startup stage.

Follow me on the web and receive insight from my channel on [twitter][twitter]. 

[twitter]:https://www.twitter.com/paulohcorreia
