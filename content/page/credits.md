---
title: Credits
subtitle:  Credit done where credit is due
comments: false
---

### Credits
 [Hugo v0.65.1 powered](http://gohugo.io/)  •  Theme by [Beautiful Jekyll](http://deanattali.com/beautiful-jekyll/) adapted to [Beautiful Hugo](https://github.com/halogenica/beautifulhugo)
