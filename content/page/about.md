---
title: About me
subtitle:  broadening people and developing horizons
comments: false
---

Welcome! Thanks for taking your time to read my blog. My name is Paulo Henrique Correia and I'm glad you are here. 

I hope that my writings can contribute to people who seeks to consciously pursue and fulfil their own desired role in society in order to attain self-realization and a sustainable world. For that I will share here some insights of my life as an entrepreneur, immigrant and explorer. 
It's gonna be all about developing people and broadening horizons!

Some topics you may find here and may be interested in:

- Self Development 
- Technology
- Entrepreneurship 


### A little of my past

Born on Belo Horizonte, Brazil, I have always been fond of a challenge. Whether it was playing sports or playing music I was looking for a solution to optmize and improve. Curiosity brough me to attend [CEFET-MG](https://www.cefetmg.br) and then [UFMG](https://ufmg.br). After a trip to Rio de Janeiro, I felt in love with getting to know different cultures and wanted to improve my career abroad. An opportunity given by FIAT Automobile brought me to Italy. I graduated at the Polytechnic University of Turin and now I hold a degree on Automotive Engineering.  one   lways asking questions and looking for answers. Loved sports when younger, used to play [Peteca](https://en.wikipedia.org/wiki/Peteca) and play guitar


