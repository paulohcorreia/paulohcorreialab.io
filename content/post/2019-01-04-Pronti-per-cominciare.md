---
title: Pronti per cominciare
subtitle: Intervista alla Radio Veronica one
date: 2019-01-04
tags: ["italiano", "casabrasiltorino"]
---

Siamo pronti per ricominciare l'anno! E quest'anno lo faciamo in grande stile. Una intervista alla Radio Veronica raccontiamo come e perché Casa Brasil Torino nasce. 


Qualcuno conosce il nostro passato festaiolo e ha pure partecipato alla tradizionale festa junina, al carnevale o al Blocco du Mundo. Quest'anno, siamo pronti a continuare con quello che abbiamo iniziato e siamo determinati a riproporre le attività ormai consolidate. 



{{< figure src="/img/20190104/festa-junina.jpg" title="Festa Junina 2018" >}}
<small>Crediti immagine: Augusto Montaruli</small>

#### Che cosa è Casa Brasil Torino?

Casa Brasil Torino è un'associazione di promozione sociale. Ma cosa vuole davvero dire questa scelta 

#### Siete passati da Warã a Casa Brasil Torino? Perché abbiamo cambiato nome?

Casa Brasil Torino vuole essere un nome più rappresentativo della comunità. 

#### Ci sono stati cambiamenti? 

Ci siamo accorti che le questioni culturali sono state


- Valorizzazione del capitale umano através de corsi, eventi, innovazione culturale e civica. 

### Attività

Presso Casa Brasil non mogliamo nulla! Le attività non dimenticano ne il bisogno di divertirsi ne il desiderio di lavorare. [tentativo di fare un gioco di parole con i verbi che sono scritti in un ordine inverso]  Previste quest'anno sono la partecipazione

### Calendario

Da quest'anno prevediamo iniziative legate all'imprenditoria e al Lavoro. 

### Dove incontrarci
